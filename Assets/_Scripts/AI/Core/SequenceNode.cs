﻿namespace AIBehaviours
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "SequenceNode", menuName = "Nodes/Sequence  ")]
    public class SequenceNode : CompositeNode
    {
        public override void OnChildNodeCompleted(bool success)
        {
            currentActiveNode.onCompletedEvent -= OnChildNodeCompleted;

            if (success)
            {
                if (currentNode == childs.Length - 1)
                    Complete(true);
                else
                    SwitchNode();
            }
            else
            {
                Complete(false);
            }
        }

        public override void SwitchNode()
        {
            // increment the current active node number
            currentNode++;

            currentActiveNode = childs[currentNode].GetNode();

            // set parent node to this
            currentActiveNode.SetParent(this);
            // subsribe to events
            currentActiveNode.onCompletedEvent += OnChildNodeCompleted;

            // init new node
            currentActiveNode.InitNode(target);
        }

        public override void Execute()
        {
            if (currentNode < childs.Length)
                currentActiveNode.Execute();
        }
    }
}