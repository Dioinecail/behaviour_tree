﻿namespace AIBehaviours
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public enum SelectType
    {
        Random,
        Strict,
        Priority
    }
    [CreateAssetMenu(fileName = "SelectorNode", menuName = "Nodes/Selector")]
    public class SelectorNode : CompositeNode
    {
        // parameters
        public SelectType selectType;

        // cache
        private HashSet<BaseNode> failedNodes;

        public override void InitNode(GameObject targetObject)
        {
            if (failedNodes == null)
                failedNodes = new HashSet<BaseNode>();
            else
                failedNodes.Clear();

            base.InitNode(targetObject);
        }

        public override void OnChildNodeCompleted(bool success)
        {
            currentActiveNode.onCompletedEvent -= OnChildNodeCompleted;

            if(success)
            {
                Complete(true);
                return;
            }
            else
            {
                failedNodes.Add(childs[currentNode]);

                if (failedNodes.Count == childs.Length)
                    Complete(false);
                else
                    SwitchNode();
            }
        }

        public override void SwitchNode()
        {
            // remove any subscriptions

            switch (selectType)
            {
                case SelectType.Random:
                    currentNode = Random.Range(0, childs.Length);
                    break;
                case SelectType.Strict:
                    currentNode++;
                    break;
                default:
                    break;
            }

            currentActiveNode = childs[currentNode].GetNode();

            // set parent node to this
            currentActiveNode.SetParent(this);
            // subsribe to events
            currentActiveNode.onCompletedEvent += OnChildNodeCompleted;

            // init new node
            currentActiveNode.InitNode(target);
        }

        public override void Execute()
        {
            if (childs.Length > 0 && currentNode < childs.Length)
                currentActiveNode.Execute();

            base.Execute();
        }
    }
}