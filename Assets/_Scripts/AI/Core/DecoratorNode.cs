﻿namespace AIBehaviours
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class DecoratorNode : BaseNode
    {
        public BaseNode child;

        private BaseNode currentActiveNode;

        public override void InitNode(GameObject targetObject)
        {
            currentActiveNode = child.GetNode();

            currentActiveNode.SetParent(this);
            currentActiveNode.InitNode(targetObject);
            currentActiveNode.onCompletedEvent += Complete;
        }

        public virtual bool ProcessResult(bool success)
        {
            return success;
        }

        public override void Complete(bool success)
        {
            currentActiveNode.onCompletedEvent -= Complete;
            // do stuff with the completed result
            success = ProcessResult(success);

            base.Complete(success);
        }

        public override void Execute()
        {
            currentActiveNode.Execute();
        }
    }
}