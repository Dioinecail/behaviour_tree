﻿namespace AIBehaviours
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class BehaviourTreeCore : MonoBehaviour
    {
        public float resetDelay = 1;

        public BaseNode treeRoot;

        private BaseNode currentNode;
        private bool isReady = true;

        private void Awake()
        {
            InitBehaviourTree();
        }

        private void Update()
        {
            if(isReady)
                ExecuteNode();
        }

        public void ExecuteNode()
        {
            if (currentNode != null)
                currentNode.Execute();
        }

        public void InitBehaviourTree()
        {
            currentNode = treeRoot.GetNode();
            if (currentNode != null)
            {
                currentNode.onCompletedEvent += OnNodeCompletedEvent;

                currentNode.InitNode(gameObject);
            }
        }

        private void ResetNodeStack()
        {
            StartCoroutine(ResetCorutine());
        }

        public void OnNodeCompletedEvent(bool success)
        {
            currentNode.onCompletedEvent -= OnNodeCompletedEvent;

            ResetNodeStack();
        }

        private IEnumerator ResetCorutine()
        {
            isReady = false;
            yield return new WaitForSeconds(resetDelay);
            isReady = true;

            InitBehaviourTree();
        }
    }
}