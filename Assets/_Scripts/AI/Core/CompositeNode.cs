﻿namespace AIBehaviours
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class CompositeNode : BaseNode
    {
        private Dictionary<string, object> persistentData = new Dictionary<string, object>();

        // referenses
        protected GameObject target;
        public BaseNode[] childs;

        // cache
        protected int currentNode = 0;
        protected BaseNode currentActiveNode;

        public abstract void SwitchNode();

        public abstract void OnChildNodeCompleted(bool success);

        public override void InitNode(GameObject targetObject)
        {
            target = targetObject;
            currentNode = 0;

            currentActiveNode = childs[0].GetNode();
            currentActiveNode.SetParent(this);
            currentActiveNode.onCompletedEvent += OnChildNodeCompleted;
            currentActiveNode.InitNode(target);
        }

        public object GetData(string id)
        {
            if (persistentData.ContainsKey(id))
                return persistentData[id];
            else
                return null;
        }

        public void SetData(string id, object data)
        {
            if (persistentData.ContainsKey(id))
                persistentData[id] = data;
            else 
                persistentData.Add(id, data);
        }
    }
}