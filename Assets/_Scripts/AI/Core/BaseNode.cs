﻿namespace AIBehaviours
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public interface INode
    {
        void Execute();
        void Complete(bool success);
        bool CheckCondition();
        void InitNode(GameObject targetObject);
        void ResetNode();

        void SetInfo(UnityEngine.Object info);
        void SetInfo(Vector3 info);
        void SetInfo(float info);
        void SetInfo(string info);
        void SetInfo(int info);
        void SetInfo(bool info);
    }

    [Serializable]
    public abstract class BaseNode : ScriptableObject, INode
    {
        public string nodeName;

        public event Action<bool> onCompletedEvent;
        public event Action onStopEvent;

        protected INode parent;

        public virtual bool CheckCondition()
        {
            return true;
        }

        public virtual void Complete(bool success)
        {
            onCompletedEvent?.Invoke(true);
        }
        public virtual void Execute()
        {
            if (CheckCondition())
                Complete(true);
        }

        public abstract void InitNode(GameObject targetObject);
        public virtual void ResetNode() { }
        public BaseNode GetNode()
        {
            return Instantiate(this);
        }

        public virtual INode GetParentNode()
        {
            return parent;
        }
        public virtual void SetParent(INode node)
        {
            parent = node;
        }

        public virtual void SetInfo(UnityEngine.Object info) { }
        public virtual void SetInfo(Vector3 info) { }
        public virtual void SetInfo(float info) { }
        public virtual void SetInfo(string info) { }
        public virtual void SetInfo(int info) { }
        public virtual void SetInfo(bool info) { }
    }
}