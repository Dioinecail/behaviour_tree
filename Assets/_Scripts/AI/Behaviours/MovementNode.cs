﻿namespace AIBehaviours
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Nodes/MovementNode")]
    public class MovementNode : LeafNode
    {
        // referenses
        private Transform transform;

        // parameters
        public bool debugPath;
        public float distanceThreshold;
        public float movementSpeed;
        public float rotationSpeed;

        // cache
        private Vector3 targetPoint = Vector3.zero;
        private Vector3 currentDirection = Vector3.forward;

        public override void InitNode(GameObject targetObject)
        {
            transform = targetObject.transform;

            if (parent != null && parent is CompositeNode)
            {
                targetPoint = (Vector3)((CompositeNode)parent).GetData("TargetPosition");
            }
        }

        public override void SetInfo(Vector3 info)
        {
            targetPoint = info;
        }

        public override void ResetNode()
        {
            targetPoint = Vector3.zero;
        }

        public override bool CheckCondition()
        {
            return Vector3.Distance(transform.position, targetPoint) < distanceThreshold;
        }

        public override void Complete(bool success)
        {
            base.Complete(success);
            Debug.LogFormat("[MovementNode.Complete({0})] Completed movement for target {1}",success , transform.name);
        }

        public override void Execute()
        {
            RotateTowardsTarget();
            Move();

            if (debugPath)
                DebugPath();

            base.Execute();
        }

        private void RotateTowardsTarget()
        {
            Vector3 direction = targetPoint - transform.position;
            direction.y = 0;
            direction.Normalize();

            Quaternion targetRotation = Quaternion.FromToRotation(currentDirection, direction);

            currentDirection = Quaternion.Lerp(Quaternion.identity, targetRotation, rotationSpeed * Time.deltaTime) * currentDirection;
            currentDirection.y = 0;
            currentDirection.Normalize();
        }

        private void Move()
        {
            transform.position += currentDirection * movementSpeed * Time.deltaTime;
        }

        private void DebugPath()
        {
            Debug.DrawLine(transform.position, targetPoint, Color.green);
        }
    }
}