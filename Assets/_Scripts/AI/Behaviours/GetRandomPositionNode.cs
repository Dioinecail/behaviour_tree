﻿namespace AIBehaviours
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Nodes/GetRandomPositionNode")]
    public class GetRandomPositionNode : LeafNode
    {
        public Vector3 minPoint;
        public Vector3 maxPoint;

        private Vector3 targetPoint = Vector3.zero;

        public override bool CheckCondition()
        {
            return (targetPoint != Vector3.zero);
        }

        public override void Execute()
        {
            float x = Random.Range(minPoint.x, maxPoint.x);
            float y = Random.Range(minPoint.y, maxPoint.y);
            float z = Random.Range(minPoint.z, maxPoint.z);

            targetPoint = new Vector3(x, y, z);

            base.Execute();
        }

        public override void Complete(bool success)
        {
            if(parent != null && parent is CompositeNode)
            {
                ((CompositeNode)parent).SetData("TargetPosition", targetPoint);
            }

            base.Complete(success);
        }
    }
}